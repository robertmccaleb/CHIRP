# CHIRP



## Marine CH to Freg config
CHIRP file with mapping from VHF Marine channel to channel and text name for the channel for the US.  I have denoted Coast Guard Channels as well as MIL channels and DONOT or CG-ONLY in the text name.  Great for a Baofeng back up radio.  I also mapped the exact channel numbers so CH16 matches the 16th slot in memory. 

Example: CH16 is 156.800 and named MAR16.  CH13 is 156.650 in the 13th saved spot but named BRIDGE

I'll add separate Local Austin TX Repeaters in the future... 

USE AT YOUROWN RISK.  THIS CHIRP FILE WILL REMOVE ALL YOUR CURRENT SETTINGS. 


https://chirp.danplanet.com/projects/chirp/wiki/Home

uvh9 file is for a Wouxon uvh9 and a few local repeaters for the ATX area
